# README #

This is a repository with few Ruby Basic Programs.

### What is this repository for? ###

This repository helps the freshers at [RubyEffect](www.rubyeffect.com) to follow the video tutorials having these examples.

### How do I get set up? ###

* Download and install Ruby.
* Open these programs with a text editor.
* save as the file with .rb extention.
* Run the code using terminal.

### Contribution guidelines ###

If you have done with few more basic programs by following tutorials please make your contribution.

### Who do I talk to? ###

* Repo owner or admin : Atchyut

All the best :+1: 